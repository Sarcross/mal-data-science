#!/usr/bin/python3

import requests
import re
import time
import argparse
from pprint import pprint

'''
    Globals
'''
URL_BASE = "https://myanimelist.net/anime/{}"

def log(msg):
    if PRETTY:
        pprint(msg)
    elif DEBUG:
        print(msg)

def get_content_from_url(url):
    log(f"get_content_from_url(): Connecting to {url}")
    headers = {
        'User-Agent': 'MAL Scraper for Purely Educational Purposes'
    }
    response = requests.get(url, headers=headers)

    return response

def extract_data(id):
    url = URL_BASE.format(id)
    response = get_content_from_url(url)
    status = response.status_code
    content = response.content

    log(f"extract_data(): Received {status} from {url}")

    if status == 404:
        log(f"extract_data(): Got {status} for {url}")
        raise Exception(f"Resource not found: {url}")
    if status != 200:
        log(f"extract_data(): Got {status} for {url}")
        raise Exception(f"Non 200 response: {url} ({status})")


    content = str(content)
    content = re.sub("\s+", ' ', content).strip()
    title = content.split('<h1 class="h1"><span itemprop="name">')[1].split('</span>')[0]
    log(f"extract_data(): Title: ( {id} ) {title}")

    # Extract more exact status with this URL - need ID + Name to get the right information
    # stats_url = re.findall("(https://myanimelist.net/anime/[a-zA-Z0-9_]*/[a-zA-Z0-9_]*/stats)", content)[0]
    stats_url = re.findall(f"(https://myanimelist.net/anime/{id}/[a-zA-Z0-9_]*/stats)", content)[0]
    # print(stats_url)

    content = content.split("<h2>Information</h2>")[1]
    content = content.split('<div class="clearfix mauto mt16" style="width:160px;padding-right:10px">')[0]
    content = content.split('\\n')

    for ndx in range(len(content)):
        content[ndx] = content[ndx].strip()

    # Remove some garbage
    del content[0]
    del content[len(content) - 1]

    dataPoints = []
    
    for ndx in range(len(content)):
        # print(str(ndx) + " | " + content[ndx])
        if "span class" in content[ndx]:
            dataPoints.append(content[ndx])
            dataPoints.append(content[ndx + 1])
        if "meta itemprop" in content[ndx]:
            dataPoints.append(content[ndx])

    for ndx in range(len(dataPoints)):
        try:
            if dataPoints[ndx] == dataPoints[ndx - 1]:
                del dataPoints[ndx]
        except IndexError:
            # Only should happen on the first index
            pass

    # for ndx in range(len(dataPoints)):
    #     print(str(ndx) + "\t| " + dataPoints[ndx])

    dataPointsIterator = 0
    dataDictionary = {}
    while dataPointsIterator < len(dataPoints):
        # print(dataPoints[dataPointsIterator])
        if "dark_text" in dataPoints[dataPointsIterator] and "Score:" not in dataPoints[dataPointsIterator]:
            field = dataPoints[dataPointsIterator].split('<span class="dark_text">')[1].split(':')[0]
            dataPointsIterator = dataPointsIterator + 1
            value = dataPoints[dataPointsIterator]
            dataPointsIterator = dataPointsIterator + 1
            dataDictionary[field] = value
        elif "Score:" in dataPoints[dataPointsIterator]:
            field = "Score"
            dataPointsIterator = dataPointsIterator + 1
            value = dataPoints[dataPointsIterator]
            dataPointsIterator = dataPointsIterator + 2
            dataDictionary[field] = value
        else:
            dataPointsIterator = dataPointsIterator + 1

    dataDictionary["Title"] = title
    if "TV" in dataDictionary["Type"]:
        log(f"extract_data(): ( {id} ) {title} is type TV")
        dataDictionary["Type"] = "TV"
    elif "OVA" in dataDictionary["Type"]:
        log(f"extract_data(): ( {id} ) {title} is type OVA")
        dataDictionary["Type"] = "OVA"
    elif "Movie" in dataDictionary["Type"]:
        log(f"extract_data(): ( {id} ) {title} is type Movie")
        dataDictionary["Type"] = "Movie"

    rating_value = {}
    for key in dataDictionary:
        log(f"extract_data(): Operating on {key}")
        value = dataDictionary[key]
        log(f"extract_data(): Got ({value})")
        if "None found" in value:
            dataDictionary[key] = "None"
        if "season" in value:
            dataDictionary[key] = value.split('">')[1].split("<")[0]
        elif "href" and "title" in value:
            fields = value.split(",")
            finalField = []
            for field in fields:
                field = re.findall('title="[\s\S]*"', field)[0]
                field = field.split('"')[1].split('"')[0]
                finalField.append(field)
            dataDictionary[key] = '"' + ','.join(finalField) + '"'
        elif "ratingValue" in value:
            # Get the ratingValue
            dataDictionary[key] = re.findall("\d{1,2}\.\d{1,2}", value)[0]
            # Rating Count temporarily removed because of MAL Holiday Theme
            # val = value.split('ratingCount">')[1].split("<")[0]
            # rating_value["Rating Count"] = value.split('ratingCount">')[1].split("<")[0]
            # print(re.findall("(\d{1,3})*,\d{1,3}|\d{1,3}", value))
            # print(re.match("(\d{1,3})*,\d{1,3}|\d{1,3}", value))
    
    if dataDictionary["Ranked"] is not None:
        dataDictionary["Ranked"] = dataDictionary["Ranked"].replace("#", "").replace("<sup>2</sup>", "")

    dataDictionary.update(rating_value)
    dataDictionary["Id"] = id

    if PRINT_DICTIONARY:
        pprint(dataDictionary)
    else:
        log(dataDictionary)
    return dataDictionary




def main():
    log("main(): Starting scrape...")
    if TARGET is not None:
        log(f"main(): Performing TARGET scrape on ID: {TARGET}")
        data = extract_data(TARGET)
        log("main(): Scrape complete.")
        return

    bad_paths = []
    data = []

    try:
        log("main(): Opening bad_paths.txt")
        bad_paths_file = open("bad_paths.txt", "r")
        log("main(): Reading bad_paths.txt...")
        for line in bad_paths_file:
            bad_paths.append(line.replace('\n', ''))
    except FileNotFoundError:
        log("main(): Could not find bad_paths.txt")
        pass

    log(f"main(): Performing RANGE scrape from ID {BEGIN} to {END}")
    for ndx in range(BEGIN, END + 1):
        log(f"main(): Gathering data for ( {ndx} ).")
        if str(ndx) not in bad_paths:
            try:
                data.append(extract_data(ndx))
                log("-----------------------------------")
                time.sleep(2.5)
            except Exception as error:
                log("main(): " + str(error))
                bad_paths.append(ndx)
                time.sleep(2.5)
        else:
            log(f"main(): Data for ( {ndx} ) does not exist.")
            # Shorter sleep for nonexistent pages
            time.sleep(1)
    log(f"main(): Scrape complete")

    log(f"main(): Writing {len(bad_paths)} bad paths to bad_paths.txt.")
    bad_paths_file = open("bad_paths.txt", "w")
    for path in bad_paths:
        bad_paths_file.write(str(path) + "\n")

    # Let's create a "pipe" separated value file instead
    ova_file = open("ova.psv", "w")
    tv_file = open("tv.psv", "w")
    movie_file = open("movie.psv", "w")

    # Write file headers
    tv_key_list = list(filter(lambda x: x["Type"] == 'TV', data))
    if len(tv_key_list) > 0:
        log(f"main(): Header to tv.psv")
        tv_file.write('|'.join(tv_key_list[0].keys()) + "\n")

    ova_key_list = list(filter(lambda x: x["Type"] == 'OVA', data))
    if len(ova_key_list) > 0:
        log(f"main(): Header to ova.psv")
        ova_file.write('|'.join(ova_key_list[0].keys()) + "\n")

    movie_key_list = list(filter(lambda x: x["Type"] == 'Movie', data))
    if len(movie_key_list) > 0:
        log(f"main(): Header to movie.psv")
        movie_file.write('|'.join(movie_key_list[0].keys()) + "\n")

    # Write .psv
    log(f"main(): Writing data to .PSV files...")
    for entry in data:
        for key in entry:
            if entry[key] == "TV":
                tv_file.write('|'.join(str(val) for val in entry.values()) + "\n")
            elif entry[key] == "OVA":
                ova_file.write('|'.join(str(val) for val in entry.values()) + "\n")
            elif entry[key] == "Movie":
                movie_file.write('|'.join(str(val) for val in entry.values()) + "\n")
    log(f"main(): Write complete.")

parser = argparse.ArgumentParser(description='MyAnimeList.net Screen Scraper for Data Science Educational Purposes',
                                 allow_abbrev=False)
parser.add_argument('-D', '--debug', action='store_true', help='Enables debug messages')
parser.add_argument('-P', '--pretty', action='store_true', help='Pretty print debug messages. Turns on debug messaging automatically.')
parser.add_argument('-p', '--print-dictionary', action='store_true', help='Prints the dictionary of each entry after data is extracted.')
parser.add_argument('-b', '--begin', type=int, help='The ID to begin scraping from. (Default = 0)', default=0)
parser.add_argument('-e', '--end', type=int, help='The ID to terminate scraping at. (Default= 10580)', default= 10580) # 10528 is the upper bound as of 12/15/2019
parser.add_argument('-t', '--target', type=int, help='The ID to target for scraping. Takes precedence over BEGIN and END. Bypasses writing to .PSV')
args = parser.parse_args()

DEBUG = args.debug
PRETTY = args.pretty
BEGIN = args.begin
END = args.end
TARGET = args.target
PRINT_DICTIONARY = args.print_dictionary
if PRETTY:
    DEBUG = True

if __name__ == '__main__':
    main()